DROP TABLE blogitem

-- Since currently most are using pgcrypto, instead of uuid_generate_v1() you can use gen_random_uuid() for a Version 4 UUID value.

-- First, enable pgcrypto in your Postgres.
create extension IF NOT EXISTS "pgcrypto";

-- Then set DEFAULT of a column to
-- DEFAULT gen_random_uuid()
CREATE TABLE blogitem (
	uuid_ uuid not null default gen_random_uuid(),
	authorid varchar NOT NULL,
	title text NOT NULL,
	content text NOT NULL,
	CONSTRAINT blogitem_pkey PRIMARY KEY (uuid_)
);
 

DROP TABLE useritem

CREATE TABLE public.useritem (
	uuid_ uuid not null default gen_random_uuid(),
	username text NOT NULL,
	userid text NOT NULL,
	password text NOT NULL,
	token text NOT NULL,
	CONSTRAINT useritem_pkey PRIMARY KEY (uuid_)
);
