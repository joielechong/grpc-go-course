package main

import (
	"context"
	"fmt"
	"log"
	"math/rand"
	"net"
	"os"
	"os/signal"
	"time"

	"github.com/jackc/pgx"
	"gitlab.com/joielechong/grpc-go-course/blog/blogpb"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/grpclog"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/reflection"
	"google.golang.org/grpc/status"

	guuid "github.com/google/uuid"
	"github.com/segmentio/ksuid"
)

var conn *pgx.Conn

type server struct{}

type blogItem struct {
	ID       string `bson:"_id,omitempty"`
	AuthorID string `bon:"author_id"`
	Content  string `bson:"content"`
	Title    string `bson:"title"`
}

type userItem struct {
	Id       string
	UserName string
	UserId   string
	Password string
	Token    string
}

func (*server) CreateBlogUser(ctx context.Context, req *blogpb.CreateUserRequest) (*blogpb.CreateUserResponse, error) {
	u := req.GetUser()

	userItem := userItem{
		UserName: u.GetUserName(),
		Password: u.GetPassword(),
	}

	user, err := addUser(&userItem)

	if err != nil {
		return nil, status.Errorf(
			codes.Internal,
			fmt.Sprintf("Internal error: %v", err),
		)
	}

	return &blogpb.CreateUserResponse{
		User: &blogpb.User{
			Id:       user.Id,
			UserName: user.UserName,
			UserId:   user.UserId,
			Password: user.Password,
			Token:    user.Token,
		},
	}, nil
}

func (*server) CreateBlog(ctx context.Context, req *blogpb.CreateBlogRequest) (*blogpb.CreateBlogResponse, error) {
	blog := req.GetBlog()

	data := blogItem{
		AuthorID: blog.GetAuthorId(),
		Title:    blog.GetTitle(),
		Content:  blog.GetContent(),
	}

	id, err := addBlog(&data)

	if err != nil {
		return nil, status.Errorf(
			codes.Internal,
			fmt.Sprintf("Internal error: %v", err),
		)
	}

	return &blogpb.CreateBlogResponse{
		Blog: &blogpb.Blog{
			Id:       id,
			AuthorId: blog.GetAuthorId(),
			Title:    blog.GetTitle(),
			Content:  blog.GetContent(),
		},
	}, nil
}

func (*server) ReadBlog(ctx context.Context, req *blogpb.ReadBlogRequest) (*blogpb.ReadBlogResponse, error) {
	blogId := req.GetBlogId()

	blogItem, err := readBlog(blogId)

	if err != nil {
		return nil, status.Errorf(
			codes.Internal,
			fmt.Sprintf("Internal error: %v", err),
		)
	}

	return &blogpb.ReadBlogResponse{
		Blog: &blogpb.Blog{
			Id:       blogItem.ID,
			AuthorId: blogItem.AuthorID,
			Title:    blogItem.Title,
			Content:  blogItem.Content,
		},
	}, nil
}

func (*server) UpdateBlog(ctx context.Context, req *blogpb.UpdateBlogRequest) (*blogpb.UpdateBlogResponse, error) {
	fmt.Println("Update blog request")
	blog := req.GetBlog()

	id := blog.Id

	prevBlog, err := readBlog(id)

	if err != nil {
		return nil, status.Errorf(
			codes.InvalidArgument,
			fmt.Sprintf("Cannot get data with id %v", id),
		)
	}

	prevBlog.AuthorID = blog.AuthorId
	prevBlog.Title = blog.Title
	prevBlog.Content = blog.Content

	err = updateBlog(prevBlog)

	if err != nil {
		return nil, status.Errorf(
			codes.NotFound,
			fmt.Sprintf("Cannot update data with id %v", id),
		)
	}

	return &blogpb.UpdateBlogResponse{
		Blog: &blogpb.Blog{
			Id:       prevBlog.ID,
			AuthorId: prevBlog.AuthorID,
			Title:    prevBlog.Title,
			Content:  prevBlog.Content,
		},
	}, nil
}

func (*server) DeleteBlog(ctx context.Context, req *blogpb.DeleteBlogRequest) (*blogpb.DeleteBlogResponse, error) {
	fmt.Println("Delete blog request")

	id := req.BlogId

	if len(id) == 0 {
		return nil, status.Errorf(
			codes.OutOfRange,
			fmt.Sprintf("Invalid id"),
		)
	}

	err := deleteBlog(id)

	if err != nil {
		return nil, status.Errorf(
			codes.NotFound,
			fmt.Sprintf("Can not delete item"),
		)
	}

	return &blogpb.DeleteBlogResponse{
		BlogId: id,
	}, nil

}

func (*server) ListBlog(req *blogpb.ListBlogRequest, stream blogpb.BlogService_ListBlogServer) error {

	items, err := getAllBlog()

	if err != nil {
		return status.Errorf(
			codes.Internal,
			fmt.Sprintf("unknow internal error: %v", err),
		)
	}

	for i := 0; i < len(items); i++ {
		item := items[i]
		stream.Send(&blogpb.ListBlogResponse{
			Blog: &blogpb.Blog{
				Id:       item.ID,
				AuthorId: item.AuthorID,
				Title:    item.Title,
				Content:  item.Content,
			},
		})
	}

	return nil
}

func main() {
	// log.SetFlags(log.LstdFlags | log.Lshortfile)

	fmt.Println("Connecting to PostgreSQL")

	var err error
	conn, err = pgx.Connect(context.Background(), os.Getenv("DATABASE_URL"))
	if err != nil {
		fmt.Fprintf(os.Stderr, "Unable connect to database: %v\n", err)
		os.Exit(1)
	}
	defer conn.Close(context.Background())

	fmt.Println("Connected to PostgreSQL")

	fmt.Println("Blog Service started")

	lis, err := net.Listen("tcp", "0.0.0.0:50051")
	if err != nil {
		log.Fatalf("Failed to listen: %v", err)
	}

	certFile := "ssl/server.crt"
	keyFile := "ssl/server.pem"

	creds, sslErr := credentials.NewServerTLSFromFile(certFile, keyFile)

	if sslErr != nil {
		log.Fatalf("Failed loading certificates: %v", sslErr)
		return
	}

	// opt := grpc.Creds(creds)
	// grpc.UnaryClientInterceptor(AuthInterceptor)
	// s := grpc.NewServer(opt)

	s := grpc.NewServer(
		grpc.Creds(creds),
		grpc.UnaryInterceptor(AuthInterceptor),
	)

	blogpb.RegisterBlogServiceServer(s, &server{})

	// Register reflection service on gRPC server.
	reflection.Register(s)

	go func() {
		if err := s.Serve(lis); err != nil {
			log.Fatalf("Failed to server: %v", err)
		}
	}()

	// Wait for control C to exit

	ch := make(chan os.Signal, 1)
	signal.Notify(ch, os.Interrupt)

	// Block until a signal is received
	<-ch
	fmt.Println("Stopping the server")
	s.Stop()
	fmt.Println("Closing the listener")
	lis.Close()

	fmt.Println("Closing PostgreSQL connection")
	conn.Close(context.Background())

	fmt.Println("End of Program")
}

func AuthInterceptor(ctx context.Context,
	req interface{},
	info *grpc.UnaryServerInfo,
	handler grpc.UnaryHandler) (interface{}, error) {

	start := time.Now()

	// Don't use authentication when user registering.
	if info.FullMethod == "/blog.BlogService/CreateBlogUser" {
		return handler(ctx, req)
	}

	meta, ok := metadata.FromIncomingContext(ctx)

	if !ok {
		return nil, grpc.Errorf(codes.Unauthenticated, "incorrect access token")

	}

	fmt.Printf("meta: %v", meta)

	if len(meta["token"]) != 1 {
		return nil, grpc.Errorf(codes.Unauthenticated, "incorrect access token")

	}

	if len(meta["id"]) != 1 {
		return nil, grpc.Errorf(codes.Unauthenticated, "incorrect access token")
	}

	fmt.Println("Get all the metadata")

	var id = meta["id"][0]
	var token = meta["token"][0]

	isValid, err := isValidUser(id, token)

	if err != nil {
		return nil, grpc.Errorf(codes.Internal, "Internal server error")
	}

	if !isValid {
		return nil, grpc.Errorf(codes.Unauthenticated, "incorrect access token")
	}

	// if meta["token"][0] != "valid-token" {
	// 	return nil, grpc.Errorf(codes.Unauthenticated, "incorrect access token")
	// }

	h, err := handler(ctx, req)
	// Logging with grpclog (grpclog.LoggerV2)

	grpclog.Infof("Request - Method:%s\tDuration:%s\tError:%v\n",
		info.FullMethod,
		time.Since(start),
		err)

	fmt.Fprintf(os.Stdout, "Request - Method:%s\tDuration:%s\tError:%v\n",
		info.FullMethod,
		time.Since(start),
		err)

	return h, err

}

func addBlog(blog *blogItem) (string, error) {
	// _, err = conn.Exec("insert into blogitem(authorid, title, content) "+
	// 	"values($1,$2,$3) returning uuid_", blog.AuthorID, blog.Title, blog.Content)

	var id string

	log.Printf("blog : + %v\n", blog)

	log.Printf("authorId: %v\n", blog.AuthorID)

	row := conn.QueryRow(context.Background(),
		"insert into blogitem(authorid, title, content) "+
			"values($1,$2,$3) returning uuid_", blog.AuthorID, blog.Title, blog.Content)

	log.Printf("row : %v\n", row)
	err := row.Scan(&id)

	if err != nil {
		fmt.Fprintf(os.Stderr, "QueryRow failed: %v\n", err)
		os.Exit(1)
	}
	return id, err
}

func isValidUser(userId string, token string) (bool, error) {

	//TODO: Save read user to in-memory database and check it from the memory
	//TODO: Later before checking to database.
	//TODO: Probably can use:
	//TODO: Redis, Tarantool, or SQLite in-memory

	row := conn.QueryRow(context.Background(),
		"select uuid_ from useritem where userId=$1 AND token=$2", userId, token)

	var id string
	log.Printf("row : %v\n", row)
	err := row.Scan(&id)

	if err != nil {
		fmt.Fprintf(os.Stderr, "QueryRow failed: %v\n", err)
		return false, err
	}

	if len(id) > 0 {
		return true, err
	}
	return false, err
}

func addUser(user *userItem) (*userItem, error) {
	//TODO: Need to salting the password

	userId, err := generateUserId()
	if err != nil {
		fmt.Fprintf(os.Stderr, "Can't generate userId: %v\n", err)
		return nil, grpc.Errorf(codes.Internal, "Internal server error")
	}

	token, err := generateToken()
	if err != nil {
		fmt.Fprintf(os.Stderr, "Can't generate token: %v\n", err)
		return nil, grpc.Errorf(codes.Internal, "Internal server error")
	}

	row := conn.QueryRow(context.Background(),
		"insert into useritem(username, userid, password, token) "+
			"values($1, $2, $3, $4) returning uuid_", user.UserName, userId, user.Password, token)

	var id string
	err = row.Scan(&id)

	if err != nil {
		fmt.Fprintf(os.Stderr, "QueryRow failed: %v\n", err)
		return nil, err
	}

	user.Id = id
	user.Token = token
	user.UserId = userId

	return user, nil
}

func random(min, max int) int {
	return rand.Intn(max-min) + min
}

func generateUserId() (string, error) {
	id, err := guuid.NewRandom()
	return id.String(), err
}

func generateToken() (string, error) {
	token, err := ksuid.NewRandom()

	return token.String(), err
}

func readBlog(blogId string) (*blogItem, error) {
	if len(blogId) == 0 {
		return nil, status.Errorf(
			codes.InvalidArgument,
			fmt.Sprintf("Id should be greater or equal with 0"),
		)
	}

	// create an empty struct
	data := &blogItem{}

	row := conn.QueryRow(context.Background(),
		"select uuid_, authorid, title, content from blogitem where uuid_=$1", blogId)

	log.Printf("row : %v\n", row)
	err := row.Scan(&data.ID, &data.AuthorID, &data.Title, &data.Content)

	if err != nil {
		fmt.Fprintf(os.Stderr, "QueryRow failed: %v\n", err)
		return nil, err
	}
	return data, err
}

func updateBlog(blogItem *blogItem) error {
	_, err := conn.Exec(context.Background(),
		"update blogitem set authorid=$1, title=$2, content=$3 where id=$4",
		blogItem.AuthorID, blogItem.Title, blogItem.Content, blogItem.ID)

	return err
}

func deleteBlog(id string) error {
	_, err := conn.Exec(context.Background(), "delete from blogitem where uuid_=$1", id)
	return err
}

func getAllBlog() ([]blogItem, error) {
	rows, _ := conn.Query(context.Background(), "select * from blogitem")

	blogItems := make([]blogItem, 5)
	for rows.Next() {
		data := blogItem{}
		err := rows.Scan(&data.ID, &data.AuthorID, &data.Title, &data.Content)
		if err != nil {
			return nil, err
		}
		blogItems = append(blogItems, data)
		fmt.Printf("%v\n", data)
	}

	return blogItems, rows.Err()
}
