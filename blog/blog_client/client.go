package main

import (
	"context"
	"fmt"
	"io"
	"log"
	"os"

	"gitlab.com/joielechong/grpc-go-course/blog/blogpb"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/metadata"
)

func main() {
	fmt.Println("Blog client")

	certFile := "ssl/ca.crt" // Certificate Authority Trust certificate
	creds, sslErr := credentials.NewClientTLSFromFile(certFile, "")

	if sslErr != nil {
		log.Fatalf("Error while loading CA trust certificate: %v", sslErr)
		return
	}

	opt := grpc.WithTransportCredentials(creds)
	cc, err := grpc.Dial("localhost:50051", opt)

	if err != nil {
		log.Fatalf("could not connect: %v", err)
	}
	defer cc.Close()

	c := blogpb.NewBlogServiceClient(cc)

	// createBlog(c)
	// readBlog(c)
	// updateBlog(c)

	// deleteBlog(c, 2)
	// listBlog(c)
	createUser(c)
}

func getMetadataContext() context.Context {
	md := metadata.Pairs("id", "21c14960-bfba-4fb0-8667-ff93de74b40c")
	md.Append("token", "1RBx4clNOBzPdZ6lpnAGRSq10k0")

	return metadata.NewOutgoingContext(context.Background(), md)
}

func createBlog(c blogpb.BlogServiceClient) {
	blog := &blogpb.Blog{
		AuthorId: "Joielechong",
		Title:    "My first blog",
		Content:  "This is content",
	}

	req := &blogpb.CreateBlogRequest{
		Blog: blog,
	}

	ctx := getMetadataContext()
	res, err := c.CreateBlog(ctx, req)

	if err != nil {
		log.Fatalf("Unexpected error: %v\n", err)
	}
	fmt.Printf("Blog has been created: %v\n", res)
}

func readBlog(c blogpb.BlogServiceClient) {
	res, err := c.ReadBlog(context.Background(),
		&blogpb.ReadBlogRequest{
			BlogId: "123",
		})

	if err != nil {
		fmt.Printf("Error happened while reading: %v", err)
	}

	fmt.Printf("Blog was read: %v", res)
}

func updateBlog(c blogpb.BlogServiceClient) {
	newBlog := &blogpb.Blog{
		Id:       "123",
		AuthorId: "Programmer",
		Title:    "New Blog",
		Content:  "Updated content",
	}

	res, err := c.UpdateBlog(context.Background(),
		&blogpb.UpdateBlogRequest{
			Blog: newBlog,
		})

	if err != nil {
		fmt.Printf("Error happened while reading: %v", err)
		os.Exit(1)
	}

	fmt.Printf("Blog was updated: %v\n", res)
}

func deleteBlog(c blogpb.BlogServiceClient, id string) {
	res, err := c.DeleteBlog(context.Background(),
		&blogpb.DeleteBlogRequest{
			BlogId: id,
		})

	if err != nil {
		fmt.Printf("Error happened while deleting: %v", err)
		os.Exit(1)
	}

	fmt.Printf("Blog with %v was deleted\n", res)
}

func listBlog(c blogpb.BlogServiceClient) {
	stream, err := c.ListBlog(context.Background(), &blogpb.ListBlogRequest{})

	if err != nil {
		log.Fatalf("Error while calling ListBlog RPC: %v", err)
	}

	for {
		res, err := stream.Recv()
		if err == io.EOF {
			break
		}

		if err != nil {
			log.Fatalf("Something happened: %v", err)
		}

		fmt.Println(res.GetBlog())
	}

}

func createUser(c blogpb.BlogServiceClient) (*blogpb.User, error) {
	user := &blogpb.User{
		UserName: "Joielechong",
		Password: "1234",
	}

	req := &blogpb.CreateUserRequest{
		User: user,
	}

	// ctx := getMetadataContext()

	res, err := c.CreateBlogUser(context.Background(), req)
	if err != nil {
		log.Fatalf("Unexpected error: %v\n", err)
		return nil, err
	}

	user = res.GetUser()
	fmt.Printf("User created: %v\n", user)

	return user, nil
}
